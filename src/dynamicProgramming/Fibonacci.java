package dynamicProgramming;

import java.util.HashMap;
import java.util.Map;

import io.StdOut;
import util.Stopwatch;

/******************************************************************************
 *  Compilation:  javac Fibonacci.java
 *  Execution:    java Fibonacci N
 *
 *  Computes and prints the first N Fibonacci numbers.
 *
 * Partly taken from: http://www.javacodegeeks.com/2014/02/dynamic-programming-introduction.html
 ******************************************************************************/

public class Fibonacci {
    public static long recursiveFib(int n) {
        if (n <= 1) return n;
        else return recursiveFib(n-1) + recursiveFib(n-2);
    }
    
    public static long memoizedFib(int n) {
    	if (n < 3) return 1;
    	    //Map to store the previous results
    	    Map<Integer,Long> computedValues = new HashMap<Integer, Long>();
    	    //The two edge cases
    	    computedValues.put(1, 1L);
    	    computedValues.put(2, 1L);
   
    	    return memoizedFib(n,computedValues);
    	}
   
	private static long memoizedFib(int n, Map<Integer, Long> computedValues) 
	{
	    if (computedValues.containsKey(n)) return computedValues.get(n);  
	    computedValues.put(n-1, memoizedFib(n-1,computedValues));
	    computedValues.put(n-2, memoizedFib(n-2,computedValues));
	    long newValue = computedValues.get(n-1) + computedValues.get(n-2);
	    computedValues.put(n, newValue);
	    return newValue;
	}
	
	public static long fibonacciDP(int n) {
	    long[] results = new long[n+1];
	    results[1] = 1;
	    results[2] = 1;
	    for (int i = 3; i <= n; i++) {
	        results[i] = results[i-1] + results[i-2];
	    }
	    return results[n];
	}



    public static void main(String[] args) {
       Stopwatch w = new Stopwatch();
       
       Fibonacci.recursiveFib(10);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(15);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(20);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(25);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(30);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(31);
       System.out.println(w.elapsedTime());
       Fibonacci.recursiveFib(32);
       System.out.println(w.elapsedTime());
       
    }

}
